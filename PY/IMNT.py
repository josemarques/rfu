# -*- coding: utf-8 -*-
#!/usr/bin/env python

#IMNT.py -> Impedance networks

import math
import cmath

##Networks methods
#2 Ports networks
def SMZM(s):# #Conversion from S2x2 matriz to Z2x2. Standar caracteristic impedance Z=50+0j Ohms
#Algorithm tested with S and Z matrixes obtained with VNA.
	Z0=50+0j
	INCS=((1-s[0][0])*(1-s[1][1]))-(s[0][1]*s[1][0])

	Z=[[0,0],[0,0]]
	Z[0][0]=Z0*(((1+s[0][0])*(1-s[1][1])+s[0][1]*s[1][0])/INCS)
	Z[1][0]=2*Z0*s[1][0]/INCS
	Z[0][1]=2*Z0*s[0][1]/INCS
	Z[1][1]=Z0*(((1-s[0][0])*(1+s[1][1])+s[0][1]*s[1][0])/INCS)

	return Z

def ZMZ1G(z):#Z matriz conversion to impedance of a 1 port impedance on a 1 port network- 1tG
#Config 1
	Z=z[0][0]
	return Z

def ZMZ12GG(z):#Z matriz conversion to impedance of a 1 port impedance on a 1 port network- 1tG
#Config 2
	Z=z[0][1]

	return Z

def ZMZ12(z):#Z matriz conversion to impedance of a 1 port impedance on a 2 port network- 1t2
#Config 3
	Z=z[0][0]+z[1][1]-(2*z[0][1])
	return Z

def SmtZm(Sr,Si, Z0r=50, Z0i=0):#Conversion from S2x2 matriz to Z2x2. Standar caracteristic impedance Z=50+0j Ohms
#Algorithm tested with S and Z matrixes obtained with VNA.
	Zr=[]
	Zi=[]

	S11r=Sr[0][0]
	S11i=Si[0][0]
	S21r=Sr[1][0]
	S21i=Si[1][0]
	S12r=Sr[0][1]
	S12i=Si[0][1]
	S22r=Sr[1][1]
	S22i=Si[1][1]

	Z11r=(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*(((S11r+1)*(1-S22r)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0r-(S11i*(1-S22r)-(S11r+1)*S22i+S12i*S21r+S12r*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*((S11i*(1-S22r)-(S11r+1)*S22i+S12i*S21r+S12r*S21i)*Z0r+((S11r+1)*(1-S22r)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)
	Z11i=(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*((S11i*(1-S22r)-(S11r+1)*S22i+S12i*S21r+S12r*S21i)*Z0r+((S11r+1)*(1-S22r)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*(((S11r+1)*(1-S22r)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0r-(S11i*(1-S22r)-(S11r+1)*S22i+S12i*S21r+S12r*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)

	Z21r=2*(S21r*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2))-S21i*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)))
	Z21i=2*(S21i*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2))+S21r*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)))

	Z12r=2*(S12r*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2))-S12i*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)))
	Z12i=2*(S12i*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2))+S12r*((((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*Z0i)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*Z0r)/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)))

	Z22r=(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*(((1-S11r)*(S22r+1)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0r-(-S11i*(S22r+1)+(1-S11r)*S22i+S12i*S21r+S12r*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)+((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*((-S11i*(S22r+1)+(1-S11r)*S22i+S12i*S21r+S12r*S21i)*Z0r+((1-S11r)*(S22r+1)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)
	Z22i=(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)*((-S11i*(S22r+1)+(1-S11r)*S22i+S12i*S21r+S12r*S21i)*Z0r+((1-S11r)*(S22r+1)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)-((-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)*(((1-S11r)*(S22r+1)+S11i*S22i+S12r*S21r-S12i*S21i)*Z0r-(-S11i*(S22r+1)+(1-S11r)*S22i+S12i*S21r+S12r*S21i)*Z0i))/(((1-S11r)*(1-S22r)-S11i*S22i-S12r*S21r+S12i*S21i)**2+(-S11i*(1-S22r)-(1-S11r)*S22i-S12i*S21r-S12r*S21i)**2)

	Zr=[[Z11r,Z21r],[Z12r,Z22r]]
	Zi=[[Z11i,Z21i],[Z12i,Z22i]]
	return [Zr,Zi]

def ZmtZ_1G(Zmr, Zmi):#Z matriz conversion to impedance of a 1 port impedance on a 1 port network- 1tG
#Config 1
	Zr=Zmr[0][0]
	Zi=Zmi[0][0]
	return [Zr,Zi]

def ZmtZ_12_GG():#Z matriz conversion to impedance of a 1 port impedance on a 1 port network- 1tG
#Config 2
	Zr=Zmr[0][1]
	Zi=Zmi[0][1]
	return [Zr,Zi]

def ZmtZ_12(Zmr, Zmi):#Z matriz conversion to impedance of a 1 port impedance on a 2 port network- 1t2
#Config 3
	Zr=Zmr[0][0]+Zmr[1][1]-(2*Zmr[0][1])
	Zi=Zmi[0][0]+Zmi[1][1]-(2*Zmi[0][1])
	return [Zr,Zi]

##Calculation methods definitions


